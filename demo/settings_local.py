DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'demo',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '192.168.56.2'
    }
}

STATIC_ROOT = '/Users/caiman/PycharmProjects/static'
STATIC_URL = '/static/'