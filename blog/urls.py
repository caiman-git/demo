from django.conf.urls import url
from blog import views

urlpatterns =[
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<category>\w+)/$', views.CategoryView.as_view(), name='category'),
    url(r'^(?P<category>\w+)/(?P<article>\w+)/$', views.ArticleView.as_view(), name='article'),
]