<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    {% block head %}
        <link rel="stylesheet" href="{{ STATIC_URL }}css/bootstrap.min.css">
        <script src="{{ STATIC_URL }}js/bootstrap.min.js"></script>
        <script src="{{ STATIC_URL }}js/jquery.js"></script>
    {% endblock %}
    <title>{% block title %}Index{% endblock %}</title>
    </head>
    <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Zalupstvo</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            {% for category in categories %}
            <li><a href="{{ url('blog:category', category.id) }}">{{ category.title }}</a></li>
            {% endfor %}
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    {% block content %}
    {% endblock %}
    </body>
</html>


