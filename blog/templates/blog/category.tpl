{% extends 'blog/base.tpl' %}
{% block title %}{{ category.title }}{% endblock %}
{% block content %}
    <div class="page-header">
      <h1>{{ category.title }} <small>бесплатно</small></h1>
    </div>
    <div class="list-group">
    {% for article in category.articles.all() %}
      <a href="{{ url('blog:article', category.id, article.id) }}" class="list-group-item">
        <h4 class="list-group-item-heading">{{  article.title }}</h4>
        <p class="list-group-item-text">читать скачать</p>
      </a>
    {% endfor %}
    </div>
{% endblock %}