{% extends 'blog/base.tpl' %}
{% block title %}{{ article.title }}{% endblock %}
{% block content %}
    <div class="page-header">
      <h1>{{ article.title }} <small>бесплатно</small></h1>
    </div>
    <div class="alert alert-success" role="alert">
    {{ article.content }}
    </div>
    <p>
    {% for tag in article.tags.all() %}
        <span class="label label-primary">{{ tag.title }}</span>
    {% endfor %}
    </p>

{% endblock %}