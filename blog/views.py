from django.views.generic import TemplateView
from blog import models

class BaseView(TemplateView):
    template_name = ''

    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)

        context.update({
            'categories': models.Category.objects.all()
        })

        return context


class IndexView(BaseView):
    template_name = 'blog/index.tpl'


class CategoryView(BaseView):
    template_name = 'blog/category.tpl'

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)

        context.update({
            'category': models.Category.objects.get(
                id=kwargs.get('category')
            )
        })

        return context


class ArticleView(BaseView):
    template_name = 'blog/article.tpl'

    def get_context_data(self, **kwargs):
        context = super(ArticleView, self).get_context_data(**kwargs)

        context.update({
            'article': models.Article.objects.get(
                id=kwargs.get('article')
            )
        })

        return context
