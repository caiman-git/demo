# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _


class Entity(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name=_(u'Название')
    )
    created = models.DateTimeField(
        verbose_name=_(u'Время создания'),
        blank=True,
        auto_now_add=True
    )
    updated = models.DateTimeField(
        verbose_name=_(u'Время изменения'),
        null=True,
        blank=True
    )

    def __unicode__(self):
        return u'{}'.format(self.title)

    class Meta:
        abstract = True


class Category(Entity):
    class Meta:
        verbose_name = _(u'Категория')
        verbose_name_plural = _(u'Категории')


class Tag(Entity):
    class Meta:
        verbose_name = _(u'Тег')
        verbose_name_plural = _(u'Теги')


class Article(Entity):
    content = models.TextField(
        verbose_name=_(u'Контент')
    )
    category = models.ForeignKey(
        to='Category',
        verbose_name=_(u'Категория'),
        related_name='articles',
        null=False
    )
    tags = models.ManyToManyField(
        to='Tag',
        verbose_name=_(u'Теги'),
        related_name='articles',
        blank=True
    )

    class Meta:
        verbose_name = _(u'Статья')
        verbose_name_plural = _(u'Статьи')
